#ifndef DOT_H
#define DOT_H
#include <cmath>
#include <vector>

using namespace std;

class Dot
{
public:
    vector <double> coords;
    Dot();
    Dot(vector <double> coords);
};

#endif // DOT_H
