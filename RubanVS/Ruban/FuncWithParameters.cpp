#include "pch.h"
#include "FuncWithParameters.h"


FuncWithParameters::FuncWithParameters(string func, double startDotVal, double delta, double gamma, double s, double e, int n)
{
	try {
		if (func == "" || delta <= 0 || n <= 0 || e <= 0) {
			throw "error";
		}
		this->func = func;
		Function newFunction(func);
		function = newFunction;
		for (int i = 0; i < function.varAmount; i++) {
			this->startDotValues.push_back(10.0);
		}
		for (int i = 0; i < function.varAmount; i++) {
			this->delta.push_back(10.0);
		}
		this->gamma = gamma;
		this->s = s;
		this->e = e;
		this->n = n;

	}
	catch (const char* msg) {
		this->func = "";
	}
	catch (std::logic_error) {
		this->func = "";
	}
}

FuncResult FuncWithParameters::doAlgorithm(int counter, int size) {
	FuncResult funcResult = Algorithm::doAlgorithm(function, startDotValues, delta, gamma, s, e, n, counter, size);
	return funcResult;
}


FuncWithParameters::~FuncWithParameters()
{
}
