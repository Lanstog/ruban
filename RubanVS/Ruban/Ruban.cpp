﻿#include "pch.h"
#include <iostream>
#include "FuncResult.h"
#include "function.h"
#include "Algorithm.h"
#include "FuncWithParameters.h"
#include <locale.h> 
#include <omp.h>
#include "UploadData.h"

using namespace std;

int main()
{
		setlocale(LC_ALL, "RUS");
		vector<FuncWithParameters> funcs = getFuncs("test.txt");
		vector<FuncResult> results;
		float fTimeStart = clock() / (float)CLOCKS_PER_SEC;
		for (int i = 0; i < funcs.size(); i++) {
			if (funcs.at(i).func != "") {
				FuncResult funcResult = funcs.at(i).doAlgorithm(i, funcs.size());
				results.push_back(funcResult);
			}
			else {
				FuncResult funcResult;
				results.push_back(funcResult);
			}
			//cout << "Точка: " << funcResult.getCoords() << "\n";
			//cout << "Значение функции: " << funcResult.getFuncValue() << "\n";
		}
		float fTimeStop = clock() / (float)CLOCKS_PER_SEC;
		printf("Длительность процесса %f секунд\n", fTimeStop - fTimeStart);
		loadResults("test.txt", funcs, results);
		system("pause");
		return 0;
}