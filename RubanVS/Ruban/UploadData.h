#include "pch.h"
#include "FuncWithParameters.h"
#include <iostream>
#include "FuncResult.h"
using namespace std;

vector<FuncWithParameters> getFuncs(string filename);
void loadResults(string filename, vector<FuncWithParameters> funcs, vector<FuncResult> results);