#include "pch.h"
#include "Algorithm.h"
#include <omp.h>
#include <iostream>
Algorithm::Algorithm()
{

}

FuncResult Algorithm::doAlgorithm(Function function, vector <double> startDotValues, vector <double> delta, double gamma, double s, double e, int n, int counter, int size) {
	mt19937 gen(time(0));
	uniform_real_distribution<> urd(-1, 1);
	int numThreads = 8;

	vector <Dot> dots(n);
	vector <double> results(n);
	Dot startDot;
	startDot.coords = startDotValues;
	vector <vector<double>> uVec;
	omp_set_num_threads(numThreads);
	double funcInStart = function.getFuncResult(startDot);
	for (int i = 0; i < function.varAmount; i++) {
		vector<double> vec;
		for (int j = 0; j < n; j++) {
			vec.push_back(urd(gen));
		}
		uVec.push_back(vec);
	}
#pragma omp parallel for
	for (int i = 0; i < n; i++) { //������������� ������� ��������� �����
		Dot addDot;
		addDot.coords = vector<double>(function.varAmount);
		for (int j = 0; j < function.varAmount; j++) {
			addDot.coords[j] = (startDot.coords[j] + delta[j] * uVec[j][i]);
		}
		dots[i] = addDot;
	}
	vector <Function> functions(numThreads);
#pragma omp parallel for
	for (int i = 0; i < numThreads; i++) {
		Function newFunction(function.funcString);
		functions[i] = newFunction;
	}
#pragma omp parallel for
	for (int i = 0; i < n; i++) { //������� �������� �������
		results[i] = (functions[omp_get_thread_num()].getFuncResult(dots[i]));
	}


	double min = DBL_MAX;
	double max = DBL_MIN;
	for (int i = 0; i < results.size(); i++) { //����� �������� � ���������, ������ �������� �������� �������
		if (min > results[i]) {
			min = results[i];
		}
		if (max < results[i]) {
			max = results[i];
		}
	}
	while (checkDelta(delta, e)) {
		vector <double> p(dots.size());
		vector <double> pNorm(p.size());

#pragma omp parallel for
		for (int i = 0; i < n; i++) { //������� �������� ����
			double g = (results[i] - min) / (max - min);
			p[i] = pow(s, -g);
		}

		double sum = 0;
#pragma omp parallel for
		for (int i = 0; i < n; i++) {
			sum = sum + p[i];
		}
#pragma omp parallel for
		for (int i = 0; i < n; i++) { //����������
			pNorm[i] = (p[i] / sum);
		}
#pragma omp parallel for
		for (int j = 0; j < function.varAmount; j++) {
			double u = 0;
			for (int i = 0; i < pNorm.size(); i++) { // ������� ����� ����������� �����
				u = u + (uVec[j][i] * pNorm[i]);
			}
			startDot.coords[j] = startDot.coords[j] + u * delta[j];
		}

		//TODO: �������� ��������� ������
#pragma omp parallel for
		for (int j = 0; j < function.varAmount; j++) {
			double u = 0;
			for (int i = 0; i < pNorm.size(); i++) {
				u = u + pow(abs(uVec[j][i]), 2.0) * pNorm[i];
			}
			delta[j] = delta[j] * gamma * pow(u, 1.0 / 2.0);
		}

		funcInStart = function.getFuncResult(startDot);
#pragma omp parallel for
		for (int i = 0; i < n; i++) { //������������� ������� ��������� �����
			Dot addDot;
			addDot.coords = vector<double>(function.varAmount);
			for (int j = 0; j < function.varAmount; j++) {
				addDot.coords[j] = (startDot.coords[j] + delta[j] * uVec[j][i]);
			}
			dots[i] = addDot;
		}
#pragma omp parallel for
		for (int i = 0; i < n; i++) { //������� �������� �������
			results[i] = (functions[omp_get_thread_num()].getFuncResult(dots[i]));
		}
		min = DBL_MAX;
		max = DBL_MIN;
		for (int i = 0; i < results.size(); i++) { //����� �������� � ���������, ������ �������� �������� �������
			if (min > results[i]) {
				min = results[i];
			}
			if (max < results[i]) {
				max = results[i];
			}
		}
		double maxE = DBL_MIN;
		for (int i = 0; i < delta.size(); i++) {
			if (delta.at(i) > maxE) {
				maxE = delta.at(i);
			}
		}
		system("cls");
		int output = floor(e / maxE * 100);
		if (output > 100) {
			output = 100;
		}
		cout << "�� ������ ������ ��������� " << counter << " ������� �� " << size << "\n��������: " << output << "%\n";
	}
	FuncResult funcResult(function.getFuncResult(startDot), startDot.coords);
	return funcResult;
}

bool Algorithm::checkDelta(vector<double> delta, double e) {
	bool check = false;
	for (int i = 0; i < delta.size(); i++) {
		if (delta[i] >= e) {
			check = true;
			break;
		}
	}
	return check;
}
