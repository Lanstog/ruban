#include "pch.h"
#include "FuncResult.h"

FuncResult::FuncResult() {

}

FuncResult::FuncResult(double funcResultValue, vector <double> coords)
{
	this->funcResultValue = funcResultValue;
	this->coords = coords;
}

double FuncResult::getFuncValue()
{
	return this->funcResultValue;
}

string FuncResult::getCoords()
{
	string result = "(" + std::to_string(this->coords[0]);
	for (int i = 1; i < this->coords.size(); i++) {
		result = result + ";" + std::to_string(this->coords[i]);
	}
	result = result + ")";
	return result;
}
