#include "pch.h"
#include "UploadData.h"

vector<FuncWithParameters> getFuncs(string filename) {
	vector<FuncWithParameters> funcs;
	ifstream file;
	file.open(filename);
	if (file.is_open()) {
		while (!file.eof()) {
			string str;
			string funcStr;
			file >> funcStr;
			double startDotVal;
			file >> str;
			startDotVal = stod(str);
			double delta;
			file >> str;
			delta = stod(str);
			double gamma;
			file >> str;
			gamma = stod(str);
			double s;
			file >> str;
			s = stod(str);
			double e;
			file >> str;
			e = stod(str);
			int n;
			file >> str;
			n = stoi(str);
			FuncWithParameters func(funcStr, startDotVal, delta, gamma, s, e, n);
			funcs.push_back(func);
		}
		file.close();
		return funcs;
	}
	else {
		file.close();
		return funcs;
	}
}

void loadResults(string filename, vector<FuncWithParameters> funcs, vector<FuncResult> results) {
	ofstream file;
	file.open(filename);
	if (file.is_open()) {
		for (int i = 0; i < funcs.size(); i++) {
			if (funcs.at(i).func != "") {
				file << funcs.at(i).func << " " << funcs.at(i).startDotValues.at(0) << " " << funcs.at(i).delta.at(0) << " " << funcs.at(i).gamma << " " << funcs.at(i).s << " " << funcs.at(i).e << " " << funcs.at(i).n << " " << results.at(i).getCoords() << " " << results.at(i).getFuncValue();
				if (i != funcs.size() - 1) {
					file << "\n";
				}
			}
			else {
				file << "������������� ������� ��� ����������";
				if (i != funcs.size() - 1) {
					file << "\n";
				}
			}
		}
		file.close();
	}
}