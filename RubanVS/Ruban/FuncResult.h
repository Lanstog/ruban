#pragma once
#include "vector"
#include <string>
using namespace std;

class FuncResult
{
public:
	double funcResultValue;
	vector <double> coords;
	FuncResult();
	FuncResult(double funcResultValue, vector <double> coords);
	double getFuncValue();
	string getCoords();
};

