#pragma once
#include "function.h"
#include "FuncResult.h"
#include "Algorithm.h"
using namespace std;
class FuncWithParameters
{
public:
	string func;
	vector <double> startDotValues;
	vector <double> delta;
	double gamma;
	double s;
	double e;
	int n;
	Function function;
	FuncWithParameters(string func, double startDotVal, double delta, double gamma, double s, double e, int n);
	FuncResult doAlgorithm(int counter, int size);
	~FuncWithParameters();
};

