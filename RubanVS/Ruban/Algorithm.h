#pragma once
#include "FuncResult.h"
#include "function.h"
#include <cfloat>
#include <omp.h>
#include <limits>
#include <random>
#include <ctime>

class Algorithm
{
public:
	Algorithm();
	static FuncResult doAlgorithm(Function function, vector <double> startDotValues, vector <double> searchArea, double gamma, double s, double e, int n, int counter, int size);
	static bool checkDelta(vector<double> delta, double e);
};

