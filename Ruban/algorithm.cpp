#include "algorithm.h"
#include "QMessageBox"
#include <omp.h>
Algorithm::Algorithm()
{

}

FuncResult Algorithm::doAlgorithm(Function function, vector <double> startDotValues, vector <double> delta, double gamma, double s, double e, int n) {
    mt19937 gen(time(0));
    uniform_real_distribution<> urd(-1, 1);

    vector <Dot> dots;
    vector <double> results;
    Dot startDot;
    startDot.coords = startDotValues;
    vector <vector<double>> uVec;
    //omp_set_num_threads(4);
    double funcInStart = function.getFuncResult(startDot);
    for (int i = 0; i < function.varAmount; i++) {
        vector<double> vec;
        for (int j = 0; j < n; j++) {
            vec.push_back(urd(gen));
        }
        uVec.push_back(vec);
    }

    //#pragma omp parallel for
    for (int i = 0; i < n; i++) { //Инициализация массива случайных точек
        Dot addDot;
        for (int j = 0; j < function.varAmount; j++) {
            addDot.coords.push_back(startDot.coords[j] + delta[j]*uVec[j][i]);
        }
        dots.push_back(addDot);
    }

    //#pragma omp parallel for
    for (int i = 0; i < dots.size(); i++) { //Подсчет значений функции
        results.push_back(function.getFuncResult(dots[i]));
    }


    double min = DBL_MAX;
    double max = DBL_MIN;
    for (int i = 0; i < results.size(); i++) { //Поиск минимума и максимума, расчет среднего значения функции
        if (min > results[i]) {
            min = results[i];
        }
        if (max < results[i]) {
            max = results[i];
        }
    }
    while (checkDelta(delta, e)) {
        //TODO: Антон, добавлять надо по идее сюда, но это надо обсудить
        vector <double> p;
        vector <double> pNorm;

        //#pragma omp parallel for
        for (int i = 0; i < dots.size(); i++) { //Подсчет значений ядер
            double g = (function.getFuncResult(dots[i])-min)/(max-min);
            p.push_back(pow(s, -g));
        }

        double sum = 0;
        for (int i = 0; i < p.size(); i++) {
            sum = sum + p[i];
        }
        for (int i = 0; i < p.size(); i++) { //Нормировка
            pNorm.push_back(p[i]/sum);
        }
        //TODO: Закончить многопоточность тут
        for (int j = 0; j < function.varAmount; j++) {
            double u = 0;
            for (int i = 0; i < pNorm.size(); i++) { // Подсчет новой центральной точки
                u = u + (uVec[j][i] * pNorm[i]);
            }
            startDot.coords[j] = startDot.coords[j] + u * delta[j];
        }

        //TODO: Обновить интервалы поиска
        for (int j = 0; j < function.varAmount; j++) {
            double u = 0;
            for (int i = 0; i < pNorm.size(); i++) {
                u = u + pow(abs(uVec[j][i]), 2.0) * pNorm[i];
            }
            delta[j] = delta[j] * gamma * pow(u, 1.0/2.0);
        }

        funcInStart = function.getFuncResult(startDot);
        for (int i = 0; i < n; i++) { //Инициализация массива случайных точек
            Dot addDot;
            for (int j = 0; j < function.varAmount; j++) {
                addDot.coords.push_back(startDot.coords[j] + delta[j]*uVec[j][i]);
            }
            dots[i]=addDot;
        }
        for (int i = 0; i < dots.size(); i++) { //Подсчет значений функции
            results[i]=function.getFuncResult(dots[i]);
        }
        min = DBL_MAX;
        max = DBL_MIN;
        for (int i = 0; i < results.size(); i++) { //Поиск минимума и максимума, расчет среднего значения функции
            if (min > results[i]) {
                min = results[i];
            }
            if (max < results[i]) {
                max = results[i];
            }
        }
    }
    FuncResult funcResult(function.getFuncResult(startDot), startDot.coords);
    return funcResult;
}

bool Algorithm::checkDelta(vector<double> delta, double e) {
    bool check = false;
    for (int i = 0; i < delta.size(); i++) {
        if (delta[i] >= e) {
            check = true;
            break;
        }
    }
    return check;
}
