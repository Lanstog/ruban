#include "funcresult.h"
#include "function.h"
#include "mainwindow.h"
#include "algorithm.h"
#include "ui_mainwindow.h"
#include "parser.cpp"
#include <iostream>
#include <cfloat>
#include <limits>
#include <random>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include "QDateTime"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    try {
        Function function(ui->funcInput->text());
        vector <double> startDotValues;
        for (int i = 0; i < function.varAmount; i++) {
            startDotValues.push_back(ui->xInput->text().toDouble());
        }
        vector <double> delta;
        for (int i = 0; i < function.varAmount; i++) {
            delta.push_back(ui->deltaXInput->text().toDouble());
        }
        double gamma = ui->gInput->text().toDouble();
        double s = ui->sInput->text().toDouble();
        double e = ui->eInput->text().toDouble();
        int n = ui->nInput->text().toInt();
        FuncResult funcResult = Algorithm::doAlgorithm(function, startDotValues, delta, gamma, s, e, n);
        ui->dotLabel->setText(funcResult.getCoords());
        ui->funcLabel->setText(QString::number(funcResult.getFuncValue()));

    } catch (const char* msg) {
        QMessageBox msgBox;
        msgBox.setText(msg);
        msgBox.exec();
        return;
    }
}



