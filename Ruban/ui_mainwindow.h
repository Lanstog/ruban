/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QLineEdit *funcInput;
    QGroupBox *groupBox;
    QLineEdit *xInput;
    QLineEdit *eInput;
    QLineEdit *deltaXInput;
    QLabel *label_5;
    QLineEdit *gInput;
    QLabel *label_2;
    QLineEdit *nInput;
    QLabel *label_4;
    QLabel *label_8;
    QLabel *label_6;
    QGroupBox *groupBox_2;
    QComboBox *comboBox;
    QLabel *label_10;
    QLineEdit *sInput;
    QGroupBox *groupBox_3;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *dotLabel;
    QLabel *funcLabel;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(453, 334);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 71, 21));
        funcInput = new QLineEdit(centralWidget);
        funcInput->setObjectName(QStringLiteral("funcInput"));
        funcInput->setGeometry(QRect(90, 20, 341, 21));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 60, 171, 181));
        xInput = new QLineEdit(groupBox);
        xInput->setObjectName(QStringLiteral("xInput"));
        xInput->setGeometry(QRect(40, 20, 113, 20));
        eInput = new QLineEdit(groupBox);
        eInput->setObjectName(QStringLiteral("eInput"));
        eInput->setGeometry(QRect(40, 80, 113, 20));
        deltaXInput = new QLineEdit(groupBox);
        deltaXInput->setObjectName(QStringLiteral("deltaXInput"));
        deltaXInput->setGeometry(QRect(40, 140, 113, 20));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 70, 21, 31));
        QFont font;
        font.setPointSize(12);
        label_5->setFont(font);
        gInput = new QLineEdit(groupBox);
        gInput->setObjectName(QStringLiteral("gInput"));
        gInput->setGeometry(QRect(40, 50, 113, 20));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 20, 41, 21));
        label_2->setFont(font);
        nInput = new QLineEdit(groupBox);
        nInput->setObjectName(QStringLiteral("nInput"));
        nInput->setGeometry(QRect(40, 110, 113, 20));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 40, 31, 31));
        label_4->setFont(font);
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 140, 21, 21));
        label_8->setFont(font);
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 100, 21, 31));
        label_6->setFont(font);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(220, 60, 211, 81));
        comboBox = new QComboBox(groupBox_2);
        comboBox->addItem(QString());
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(10, 20, 191, 22));
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(10, 50, 16, 21));
        label_10->setFont(font);
        sInput = new QLineEdit(groupBox_2);
        sInput->setObjectName(QStringLiteral("sInput"));
        sInput->setGeometry(QRect(50, 50, 151, 20));
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(220, 150, 211, 71));
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(10, 20, 47, 20));
        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(10, 40, 111, 21));
        dotLabel = new QLabel(groupBox_3);
        dotLabel->setObjectName(QStringLiteral("dotLabel"));
        dotLabel->setGeometry(QRect(60, 20, 141, 20));
        funcLabel = new QLabel(groupBox_3);
        funcLabel->setObjectName(QStringLiteral("funcLabel"));
        funcLabel->setGeometry(QRect(120, 40, 81, 21));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(300, 240, 131, 31));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 453, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "\320\244\321\203\320\275\320\272\321\206\320\270\321\217:", nullptr));
        funcInput->setText(QApplication::translate("MainWindow", "7 * (x1-2)^2 + 7*(x2-3)^2 + 7*(x3-3)^2", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213", nullptr));
        xInput->setText(QApplication::translate("MainWindow", "10", nullptr));
        eInput->setText(QApplication::translate("MainWindow", "0.001", nullptr));
        deltaXInput->setText(QApplication::translate("MainWindow", "20", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "<html>&epsilon;</html>", nullptr));
        gInput->setText(QApplication::translate("MainWindow", "1", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "x", nullptr));
        nInput->setText(QApplication::translate("MainWindow", "1000", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "<html>&gamma;</html", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "<html>&Delta;</html>x", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "<html>&eta;</html>", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "\320\257\320\264\320\265\321\200\320\275\320\260\321\217 \321\204\321\203\320\275\320\272\321\206\320\270\321\217", nullptr));
        comboBox->setItemText(0, QApplication::translate("MainWindow", "s^(-g)", nullptr));

        label_10->setText(QApplication::translate("MainWindow", "S", nullptr));
        sInput->setText(QApplication::translate("MainWindow", "100", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "\320\242\320\276\321\207\320\272\320\260: ", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265 \321\204\321\203\320\275\320\272\321\206\320\270\320\270:", nullptr));
        dotLabel->setText(QString());
        funcLabel->setText(QString());
        pushButton->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\277\321\203\321\201\320\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
