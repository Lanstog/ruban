#ifndef FUNCRESULT_H
#define FUNCRESULT_H
#include "vector"

#include <QString>
using namespace std;
class FuncResult
{
public:
    double funcResultValue;
    vector <double> coords;
    FuncResult(double funcResultValue, vector <double> coords);
    double getFuncValue();
    QString getCoords();
};

#endif // FUNCRESULT_H
