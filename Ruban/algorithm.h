#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "funcresult.h"
#include "function.h"
#include <cfloat>
#include <limits>
#include <random>
#include <ctime>

using namespace std;

class Algorithm
{

public:
    Algorithm();
    static FuncResult doAlgorithm(Function function, vector <double> startDotValues, vector <double> searchArea, double gamma, double s, double e, int n);
    static bool checkDelta(vector<double> delta, double e);
};

#endif // ALGORITHM_H
