#include "exprtk.hpp"


template <typename T>
/**
 * @brief parse_func Преобразует функцию строкового вида в мат.выражение и высчитывает результат функции в точке
 * @param func_string Функция в строковом виде
 * @param params Массив параметров(точка) в том порядке, в котором находятся переменные в функции
 * @return Значение функции в заданной точке
 */
double parse_func(std::string func_string, std::vector<T> params) {
    typedef exprtk::symbol_table<T> symbol_table_t;
    typedef exprtk::expression<T>     expression_t;
    typedef exprtk::parser<T>             parser_t;

    T x, y;

    symbol_table_t symbol_table;
    symbol_table.add_variable("x", x);
    symbol_table.add_variable("y", y);
    symbol_table.add_constants();

    expression_t expression;
    expression.register_symbol_table(symbol_table);

    x = T(params[0]);
    y = T(params[1]);
    parser_t parser;
    parser.compile(func_string, expression);

    return double(expression.value());
}
