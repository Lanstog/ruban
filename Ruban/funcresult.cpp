#include "funcresult.h"

FuncResult::FuncResult(double funcResultValue, vector <double> coords)
{
    this->funcResultValue = funcResultValue;
    this->coords = coords;
}

double FuncResult::getFuncValue()
{
    return this->funcResultValue;
}

QString FuncResult::getCoords()
{
    QString result = "(" + QString::number(this->coords[0]);
    for (int i = 1; i < this->coords.size(); i++) {
        result = result + ";" + QString::number(this->coords[i]);
    }
    result = result + ")";
    return result;
}
