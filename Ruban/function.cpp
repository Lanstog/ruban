#include "function.h"
#include <regex>
#include <unordered_set>

Function::Function(QString funcString)
{
    this->funcString = funcString;
    this->variables = this->getUnknownsVariable(funcString.toUtf8().constData());
    varAmount = this->variables.size();
    params_value = new double[varAmount];
    for (int i = 0; i < varAmount; i++) {
        symbol_table.add_variable(this->variables[i], params_value[i]);
    }
    symbol_table.add_constants();
    expression.register_symbol_table(symbol_table);
    Dot dot = Dot(vector<double>(varAmount, 1));
    double res = getFuncResult(dot);
    if (isnan(res)) {
        throw "Некорректная форма записи уравнения.";
    }
}

double Function::getFuncResult(Dot dot)
{
    return parse_func(this->funcString.toUtf8().constData(), dot.coords);
    //return 7 * (dot.coords[0]-2) * (dot.coords[0]-2) + 7 * (dot.coords[1]-3) * (dot.coords[1]-3);
}

double Function::parse_func(std::string func_string, std::vector<double> params) {
    for (int i = 0; i < varAmount; i++) {
        params_value[i] = params[i];
    }

    parser_t parser;
    parser.compile(func_string, expression);

    return double(expression.value());
}


vector<string> Function::getUnknownsVariable(string func) {
    smatch matches;
    regex expr("x\\d");
    unordered_set<string> myset;
    while (regex_search(func, matches, expr)) {
        myset.insert(matches[0]);
        func = matches.suffix().str();
    }
    vector<string> strVec(myset.begin(), myset.end());
    return strVec;
}
