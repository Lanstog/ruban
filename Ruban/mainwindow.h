#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "iostream"
#include "vector"
#include <QMessageBox>
#include "QString"
#include "dot.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    
private:
    Ui::MainWindow *ui;
    time_t time = 0;
};

#endif // MAINWINDOW_H
