#ifndef FUNCTION_H
#define FUNCTION_H

#include "dot.h"
#include "exprtk.hpp"
#include <QString>
#include <regex>
#include <unordered_set>

using namespace std;

class Function
{
private:
    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double>     expression_t;
    typedef exprtk::parser<double>             parser_t;
    QString funcString;
    vector<string> variables;
    double *params_value;
    expression_t expression;
    symbol_table_t symbol_table;
public:
    int varAmount;
    vector<string> getUnknownsVariable(string func);
    double getFuncResult(Dot dot);
    Function(QString funcString);
    double parse_func(std::string func_string, std::vector<double> params);
};

#endif // FUNCTION_H
